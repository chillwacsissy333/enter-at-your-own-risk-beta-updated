PhD Bigger Crafting Queue - 1.1
This simple mod expands the players crafting queue from 4 slots to 10 slots. 

I made this mod because I was always filling my queue when working my way through a POI and having to wait for a slot to become available. Other mods are available that gave me five slots which weren't much help. Seriously what's one more slot going to do? But six more? Now that really helps. 

This mod works best with stedman420's Simple UI - BiggerBackpack120-PlayerBuiltStorage mod. It just makes everything fit better on screen but it is not required.

If you use the mod and enjoy it, please feel free to endorse it and leave any feedback. 

:: REQUIREMENTS::
• Version 17.4
• Version 18


:: FEATURES::
• Expands the Player's crafting queue to 10 slots

